#Antes que nada y si te pasa como ami que sale muchos errores en session, no te preocupes creo que es un error de el editor de texto por que esta funcional


#1- instala sqlalchemy en la consola $ pip install SQLAlchemy
#2- descarga e instala mariadb https://mariadb.org/download/
#3- abre HeidiSQL que se instala al instalar mariadb
#3- crea un nuevo proyecto con su usuario y contraseña *IMPORTANTE: establece permisos para que pueda interactuar con la tabla, requiere reinicio de el programa

#importar lo que usaremos
desde sqlalchemy import create_engine,MetaData,Column, String,Table
de sqlalchemy. ext. declarative_base de importación declarativa
de sqlalchemy. orm import sessionmaker
desde sqlalchemy import database_exists, create_database

#declara la "base/modelo/tabla" esto lo utiliza para taransformar esa clase a una tabla sql 
Base = declarative_base() 

#crearemos una clase que sera el modelo de la tabla palabra, con el  podemos llamar a todas la colummna y valores de la tabla
clase PalabraModel (Base):
    __tablename__= "myTableName"
 palabra = Columna(Cadena,primary_key=Verdadero)
 definicion = Columna(Cadena)
 def __init__(self, palabra,definicion):
 yomismo. palabra = palabra
 yomismo. definicion = definicion

#esta es una funcion que al llamarlo junto al engine nos creara la tabla 
def createTable(motor): 
 meta = Metadatos()
 mydb = Tabla(
    'myTableName',meta,
 Columna('palabra',String(1000),primary_key=True),
 Columna('definicion',String(1000)))
 meta. create_all(motor)

#crearemos una clase para poder usar mas facil sqlalchemy se llamara ORM
clase ORM():
    # requiere el nombre de la tabla y el dialegto de la base de datos, hay muchas pero usaremos el dialecto de mariadb que esta basado en mysql
    def __init__(self,nameTable,dialect): 
        # concatenamos .db alo que el usuario introduzca, lo requerimos para el dialecto
 yomismo. nameTable = nameTable+'.db' 
        #Esto concatenara el dialecto introducido mas el nombre de la tabla <Dialecto+MyTableName>
        #necesitamos el dialecto para que el engine sepa manejar las tablas con su respectivo sql; es como decir motor trabaja con disel
 yomismo. dialecto = dialecto+yo. nameTable
        #inicializamos/encendemos el motor pasandole el parametro dialecto
 yomismo. motor = create_engine(self. dialecto) 

        #este es un simple verificador que crea la base de datos si no existe; utiliza la funcion creada anteriormente
 si no database_exists(self. motor. url): 
 create_database(yo. motor. url)
 createTable(self. motor)


 yomismo. session = sessionmaker(bind = self. engine)() # iniciamos session con el engine para manejar la base de datos

    # Estas funciones utilizaran la session para realizar distintas operaciones sql

    def agregarPalabra(self,palabra,definicion):
 yomismo. sesión. add(PalabraModel(palabra,definicion))
 yomismo. sesión. comprometerse()

    def obtenerPalabra(self,palabra):
 resultado = yo. sesión. consulta(PalabraModel). filtro(PalabraModel. palabra == palabra). uno()
 print('Palabra:'+resultado. palabra+'Definición:'+resultado. definicion)

    def obtenerTodo(self):
 resultado = yo. sesión. consulta(PalabraModel). todos()
        for informacion in resultado:
 print('Palabra:'+informacion. palabra+'Definicion:'+informacion. definicion+'\n')

    def editarPalbra(self,palabra,newPalabra):
 resultado = yo. sesión. consulta(PalabraModel). filtro(PalabraModel. palabra == palabra). uno()
 resultado. palabra = nuevoPalabra
 yomismo. sesión. añadir(resultado)
 yomismo. sesión. comprometerse()

    def editarDefinicion(self,palabra,definicion):
 resultado = yo. sesión. consulta(PalabraModel). filtro(PalabraModel. palabra == palabra). uno()
 resultado. definicion = definicion
 yomismo. sesión. añadir(resultado)
 yomismo. sesión. comprometerse()

    def borrarPalabra(self,palabra):
 resultado = yo. sesión. consulta(PalabraModel). filtro(PalabraModel. palabra == palabra). uno()
 yomismo. sesión. suprimir(resultado)
 yomismo. sesión. comprometerse()

#este es el dialecto para mariadb requiere <nombredeUsuario><password>es importante que este usuario tenga permisos para trabajar con la tabla
dialectoMARIADB = 'mysql+pymysql://myUserName:mypass@localhost/'
dialectoSQLITE = 'sqlite:///'
#sql sera nuestro manager de orm
#DIALECTO PARA SQLITE
#sql = ORM('MyTableName',dialectoSQLITE)
#DIALECTO PARA MARIADB
sql = ORM('MyTableName',dialectoMARIADB)
#Estos son algunos ejemplo de lo que puedes hacer
#sql.agregarPalabra('newPalabra','newDefinicion')
#sql.editarPalbra('palabra','palabraEditada')
#sql.editarDefinicion('palabra','definicionEditada')
#sql.obtenerTodo()
#sql.borrarPalabra('palabra')

#RESUMEN
#En resumen los ORM es una especie de traductor de Sintaxis de lenguaje sql, o lo puedes visaulizar como una motor que transforma un modelo
#a una tabla sql

#NOTA: esto es un ejemplo practico si quieres saber mas de aspectos tecnicos y ver las buenas practicas; ver la documentacion oficial